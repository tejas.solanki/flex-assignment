import React, { useState} from "react";
import { View, TouchableOpacity, ViewBase, StyleSheet, Text, FlatList, ScrollView, Dimensions} from "react-native";
import ColorBar from "./colorBar";


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function Main() {

    const [data, setData] = useState([
        // {id: "1",color: "black"},
        // {color: "blue"},
        // {color: "red"},
    ]);

    return (
    <View style = {{flex:1}}>
        <ScrollView style = {{maxHeight:"70%", minHeight:"70%"}}> 
            <View style={styles.mainView}>
                {/* <FlatList
                data={}
                renderItem={}
                // keyExtractor={(item) => item.id}
                // extraData={selectedId}
            /> */}
                
                {data && data.map((prev, index) => {
                    function spliceData() {
                        // const newData = data.splice(index,1);
                        // console.log(data);
                        // setData(data);
                        const newData = [...data];
                        newData.splice(index, 1);
                        console.log(newData);
                        setData(newData);
                    }
                    return (
                        <View key={index}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 10 }}>
                                <View style={{ backgroundColor: prev.color, height: 50, width: "80%", borderRadius: 20 }}></View>
                                <View>
                                    <TouchableOpacity
                                        onPress={() => spliceData()}>
                                        <View style={{ borderRadius:50,backgroundColor: "yellow", height: 50, width: 50, justifyContent:'center', alignItems:'center'}}>
                                            <Text style ={{fontSize:10,fontWeight:'bold'}}>DELETE</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View> 
                    )

                })}
            </View>
            </ScrollView>
            
                <View style = {{display:'flex',justifyContent:'center',alignItems:'center'}}>
                    <View style = {{justifyContent:'flex-start'}}>
                    <Text style={{ fontSize: 30,marginLeft:"4%",marginBottom: 10}}>ADD ROW</Text>
                    <View style={styles.mainView2}>
                        <TouchableOpacity
                            onPress={() => setData(prev => [...prev, { color: "pink" }])}>
                            <View style={styles.pink}></View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setData(prev => [...prev, { color: "orange" }])}>
                            <View style={styles.orange}></View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setData(prev => [...prev, { color: "blue" }])}>
                            <View style={styles.blue}></View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setData(prev => [...prev, { color: "green" }])}
                        >
                            <View style={styles.green}></View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setData(prev => [...prev, { color: "gray" }])}>
                            <View style={styles.gray}></View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => setData(prev => [...prev, { color: "black" }])}>
                            <View style={styles.black}></View>
                        </TouchableOpacity>
                        </View>
                    </View>
                </View>
            
        
     </View>
    );
};

const styles = StyleSheet.create({
    mainView: {
        // height:windowHeight/2,
        // minHeight:windowHeight/1.3,
        // maxHeight:windowHeight/1.3,
        marginLeft: 20,
        // flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
     
    mainView2: {
        zIndex:1,
        // marginTop: "150%",
        bottom:0,
        // position:'relative',
        marginLeft: 20,
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        // position:'fixed'
    },
    stackBar: {
        backgroundColor: "red",
        height: 50,
        width: "80%",
        borderRadius: 20

    },
    pink: {
        marginRight: 10,
        backgroundColor: 'pink',
        height: 50,
        width: 50,
        borderRadius: 50
    },
    orange: {
        marginRight: 10,
        backgroundColor: 'orange',
        height: 50,
        width: 50,
        borderRadius: 50
    },
    blue: {
        backgroundColor: 'blue',
        height: 50,
        width: 50,
        borderRadius: 50,
        marginRight: 10,

    },
    green: {
        backgroundColor: 'green',
        height: 50,
        width: 50,
        borderRadius: 50,
        marginRight: 10,

    },
    gray: {
        backgroundColor: 'gray',
        height: 50,
        width: 50,
        borderRadius: 50,
        marginRight: 10,

    },
    black: {
        backgroundColor: 'black',
        height: 50,
        width: 50,
        borderRadius: 50,
        marginRight: 10,

    }
});

export default Main;